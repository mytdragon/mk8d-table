import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import ReactDOM from 'react-dom';
import FlashMessage from 'react-flash-message';

import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import LoadingSpinner from '../../components/LoadingSpinner/LoadingSpinner';

class RegisterContainer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoggedIn: false,
            isRegistered: false,
            error: '',
            errorMessage: '',
            formSubmitting: false,
            user: {
                pseudo: '',
                email: '',
                password: '',
                password_confirmation: '',
            }
        };

        let state = localStorage['appState'];
        if (state) {
            let AppState = JSON.parse(state);
            this.state.isLoggedIn = AppState.isLoggedIn;
            this.state.user = AppState.user;
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.loadingSpinner = React.createRef();
    }

    componentDidMount() {
        // Redirect to home if already logged
        if (this.state.isLoggedIn) {
            return this.props.history.push('/');
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        // Redirect to logging page if have been registered
        if (this.state.isRegistered && !this.state.isLoggedIn) {
            return this.props.history.push('/login');
        }
    }

    handleChange(e) {
        let name = e.target.name;
        let value = e.target.value;
        this.setState(prevState => ({
            user: {
                ...prevState.user,
                [name]: value
            }
        }));
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({formSubmitting: true});
        this.loadingSpinner.current.show();

        let userData = this.state.user;
        axios.post("/api/auth/signup", userData)
        .then(response => {
            return response;
        })
        .then(json => {
            let appState = {
                isRegistered: true,
            };
            localStorage['appState'] = JSON.stringify(appState);

            this.setState({
                isRegistered: appState.isRegistered
            });

            this.loadingSpinner.current.hide();
        })
        .catch(error => {
            if (error.response) {
                // The request was made and the server responded with a status code that falls out of the range of 2xx
                let err = error.response.data;
                this.setState({
                    error: err.message,
                    errorMessage: err.errors,
                    formSubmitting: false
                })
            }
            else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser
                // and an instance of http.ClientRequest in node.js
                let err = error.request;
                this.setState({
                    error: err,
                    formSubmitting: false
                })
            }
            else {
                // Something happened in setting up the request that triggered an Error
                let err = error.message;
                this.setState({
                    error: err,
                    formSubmitting: false
                })
            }

            this.loadingSpinner.current.hide();
        })
        .finally(this.setState({error: ''}));
    }

    render() {
        let errorMessage = this.state.errorMessage;
        let errors = [];
        Object.values(errorMessage).forEach((value) => (
            errors.push(value)
        ));

        return (
            <div>
                <Header userData={this.state.user} userIsLoggedIn={false}/>

                <main className="container py-5">
                    <div className="row justify-content-center">
                        <div className="col-lg-5 col-md-8 col-10">
                            <h2>Register</h2>

                            {
                                this.state.error ?
                                    <FlashMessage duration={900000} persistOnHover={true}>
                                        <div className={"alert alert-danger"}>
                                            Error: {this.state.error}
                                            <ul>
                                                {errors.map((item, i) => (
                                                    <li key={i}>{item}</li>
                                                ))}
                                            </ul>
                                        </div>
                                    </FlashMessage>
                                : ''
                            }

                            <form className="modern-form" onSubmit={this.handleSubmit}>
                                <div className="form-group">
                                    <input id="pseudo" type="text" name="pseudo" placeholder="Pseudo" className="form-control" required onChange={this.handleChange}/>
                                </div>
                                <div className="form-group">
                                    <input id="email" type="email" name="email" placeholder="E-mail" className="form-control" required onChange={this.handleChange}/>
                                </div>
                                <div className="form-group">
                                    <input id="password" type="password" name="password" placeholder="Password" className="form-control" required onChange={this.handleChange}/>
                                </div>
                                <div className="form-group">
                                    <input id="password_confirmation" type="password" name="password_confirmation" placeholder="Confirm Password" className="form-control" required onChange={this.handleChange} />
                                </div>
                                <button disabled={this.state.formSubmitting} type="submit" name="singlebutton" className="btn btn-primary-dark  btn-block" style={{display: this.state.formSubmitting ? "none" : "block"}}>Register!</button>
                                <LoadingSpinner ref={this.loadingSpinner}/>
                            </form>

                            <p>
                                Already have an account? <Link to="/login">Log in</Link>
                            </p>
                        </div>
                    </div>
                </main>

                <Footer/>
            </div>
        )
    }
}

export default withRouter(RegisterContainer);
