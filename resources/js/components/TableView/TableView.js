import React, { Component } from 'react';
import collect from 'collect.js';

import './TableView.css';

class TableView extends Component {
    constructor(props) {
        super(props);

        this.state = this.props.table;
    }

    updateTable(table) {
        this.setState(table);
    }

    render() {
        var races = this.state.races ? this.state.races.map(function (race, index){
            return <tr key={index}>
                <td className={"bg-team " + race.vote}></td>
                <td>{race.track}</td>
                <td className="bg-race-score">{race.home_score}</td>
                <td className="bg-race-score">{race.away_score}</td>
                <td>{race.home_score - race.away_score}</td>
            </tr>
        }) : "";

        var players = collect(this.state.home_team.players).map(function(player, index) {
            player.team = 'home';
            return player;
        });
        collect(this.state.away_team.players).map(function(player, index) {
            player.team = 'away';
            return player;
        }).each(function (player) {
            players.push(player);
        });
        players = players.sortByDesc('score').map(function(player, index){
            return <tr key={index}>
                <td className={"bg-team " + player.team}></td>
                <td>{player.name} {player.races ? '('+player.races+')' : ''}</td>
                <td>{player.score}</td>
            </tr>;
        });

        return (
            <div className="mk-table-container mx-auto">
                <div className="mk-table">
                    <div className="header">
                        <div className="row">
                            <div className="col-2 header-img">
                                <img src="/img/rosa.png" alt="rosalina"/>
                            </div>
                            <div className="col-8 texts">
                                <span className="header-teams">{this.state.home_team.name} VS {this.state.away_team.name}</span>
                                <br/>
                                <span className="header-details">{this.state.category} {this.state.tag} {this.state.date}</span>
                            </div>
                            <div className="col-2 header-img">
                                <img src="/img/luma.png" alt="luma"/>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-auto players">
                            <table className="sub-table">
                                <colgroup>
                                    <col width="20"/>
                                    <col width="130"/>
                                    <col width="50"/>
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th colSpan="2">Players</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {players}
                                </tbody>
                            </table>
                        </div>

                        <div className="col-auto war">
                            <table className="sub-table">
                                <colgroup>
                                    <col width="20"/>
                                    <col width="154"/>
                                    <col width="50"/>
                                    <col width="50"/>
                                    <col width="50"/>
                                </colgroup>
                                <thead>
                                        <tr>
                                            <th colSpan="2">Races</th>
                                            <th>{this.state.home_team.tag}</th>
                                            <th>{this.state.away_team.tag}</th>
                                            <th>+/-</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {races}

                                        <tr className="tr-penalty">
                                            <th colSpan="2">Penalty</th>
                                            <td>{this.state.home_team.penalty}</td>
                                            <td>{this.state.away_team.penalty}</td>
                                            <td></td>
                                        </tr>

                                        <tr className="tr-total">
                                            <th colSpan="2">Total</th>
                                            <td>{this.state.home_team.score}</td>
                                            <td>{this.state.away_team.score}</td>
                                            <td>{this.state.home_team.score - this.state.away_team.score}</td>
                                        </tr>
                                    </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default TableView;
