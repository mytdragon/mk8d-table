<?php

namespace App\Http\Controllers;

use App\Models\War;
use App\Models\Category;
use App\Models\Track;
use App\Models\Team;
use App\Models\Player;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Resources\War as WarResource;


class WarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return WarResource::collection(War::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'table' => 'required|array',
            'table.home_team' => 'required|array',
            'table.home_team.name' => 'required|string|between:3,50',
            'table.home_team.tag' => 'required|string|between:1,9',
            'table.home_team.penalty' => 'integer|min:1|nullable',
            'table.home_team.score' => 'required|integer',
            'table.home_team.players' => 'required|array',
            'table.home_team.players.*.name' => 'required|string|max:30',
            'table.home_team.players.*.score' => 'required|integer|min:1',
            'table.home_team.players.*.races' => 'integer|min:1|max:12|nullable',
            'table.away_team' => 'required|array',
            'table.away_team.name' => 'required|string|between:3,50',
            'table.away_team.tag' => 'required|string|between:1,9',
            'table.away_team.penalty' => 'integer|min:1|nullable',
            'table.away_team.score' => 'required|integer',
            'table.away_team.players' => 'required|array',
            'table.away_team.players.*.name' => 'required|string|max:30',
            'table.away_team.players.*.score' => 'required|integer|min:1',
            'table.away_team.players.*.races' => 'integer|min:1|max:12|nullable',
            'table.races' => 'required|array',
            'table.races.*.no' => 'required|integer|max:12',
            'table.races.*.track' => 'required|string|max:50',
            'table.races.*.vote' => 'required|string|in:home,away',
            'table.races.*.home_score' => 'required|integer',
            'table.races.*.away_score' => 'required|integer',
            'table.date' => 'required|date',
            'table.category' => 'required|string|max:10',
            'table.tag' => 'required|string|max:20'
        ]);

        if ($validator->fails()) {
            return response([
                'message' => $validator->errors()
            ], 400);
        }

        $table = $request->all()['table'];

        $date = \DateTime::createFromFormat('d.m.Y', $table['date']);
        $date->setTime(0, 0, 0);

        $category = Category::where('name', $table['category'])->first() ?? Category::create([
            'name' => $table['category']
        ]);
        $category->refresh();

        $homeTeam = Team::where('name', $table['home_team']['name'])->first() ?? Team::create([
            'tag' => $table['home_team']['tag'],
            'name' => $table['home_team']['name']
        ]);
        $homeTeam->refresh();

        $awayTeam = Team::where('name', $table['away_team']['name'])->first() ?? Team::create([
            'tag' => $table['away_team']['tag'],
            'name' => $table['away_team']['name']
        ]);
        $awayTeam->refresh();

        $players = [];
        foreach ($table['home_team']['players'] as $player) {
            $players[] = [
                'model' => (Player::where('name', $player['name'])->first() ?? Player::create([
                    'name' => $player['name']
                ]))->refresh(),
                'team' => $homeTeam,
                'score' => $player['score'],
                'races' => $player['races']
            ];
        }
        foreach ($table['away_team']['players'] as $player) {
            $players[] = [
                'model' => (Player::where('name', $player['name'])->first() ?? Player::create([
                    'name' => $player['name']
                ]))->refresh(),
                'team' => $awayTeam,
                'score' => $player['score'],
                'races' => $player['races']
            ];
        }

        $races = [];
        foreach ($table['races'] as $race) {
            $races[] = [
                'no' => $race['no'],
                'track' => Track::where('name', $race['track'])->firstOrFail(),
                'vote' => $race['vote'] == 'home' ? $homeTeam : $awayTeam,
                'home_score' => $race['home_score'],
                'away_score' => $race['away_score']
            ];
        }

        $war = new War();
        $war->date = $date;
        $war->category()->associate($category);
        $war->tag = $table['tag'];
        $war->homeTeam()->associate($homeTeam);
        $war->home_score = $table['home_team']['score'];
        $war->home_penalty = $table['home_team']['penalty'] ?? 0;
        $war->awayTeam()->associate($awayTeam);
        $war->away_score = $table['away_team']['score'];
        $war->away_penalty = $table['away_team']['penalty'] ?? 0;
        $war->save();

        foreach ($players as $player) {
            $war->players()->save($player['model'], [
                'score' => $player['score'],
                'races' => $player['races'] ?? config('mk8dx.nb_races'),
                'team_id' => $player['team']->id,
            ]);
        }

        foreach ($races as $race) {
            $war->races()->save($race['track'], [
                'no' => $race['no'],
                'vote_team_id' => $race['vote']->id,
                'home_score' => $race['home_score'],
                'away_score' => $race['away_score']
            ]);
        }

        return response([
            'message' => 'War successfully added!'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\War  $war
     * @return \Illuminate\Http\Response
     */
    public function show(War $war)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\War  $war
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, War $war)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\War  $war
     * @return \Illuminate\Http\Response
     */
    public function destroy(War $war)
    {
        //
    }
}
