import React, { Component } from 'react';
import domtoimage from 'dom-to-image';
import collect from 'collect.js';
import moment from 'moment';
import axios from 'axios';
import { WebhookClient } from 'discord.js';

import './TableMaker.css';
import TableView from '../TableView/TableView';

const INIT_TABLE = {
    'home_team' : {
        'name' : null,
        'tag' : null,
        'penalty' : null,
        'score' : null,
        'players' : []
    },
    'away_team' : {
        'name' : null,
        'tag' : null,
        'penalty' : null,
        'score' : null,
        'players' : []
    },
    'races' : [],
    'date' : moment().format('DD.MM.YYYY'),
    'category' : null,
    'tag' : null
};

const POINTS = [15, 12, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1];
const POINTS_PER_RACE = 82;
const NB_RACES = 12;

const EXAMPLE_WAR = '#cat:Friendly\r\n#tag:#1\r\n#date:01.01.2019\r\n\r\nA - Team A - 10\r\nAlice 120\r\nBilly 101\r\nCarol 78\r\nDerek 78\r\nEllen 72\r\nFrank 61\r\n\r\nB - Tean B\r\nGrant 102\r\nHenry 40+50\r\nIsaac 80\r\nJames 78\r\nKaren 64\r\nLucas 58\/11 \r\nOwen 2\/1\r\n\r\nrsl home 1 2 7 8 11 12\r\nds home 1 2 3 6 8 12\r\nrmc away 5 6 7 8 9 10\r\nrttc away 5 6 7 8 9 11\r\ndcl home 1 3 4 5 6 7\r\ndyc away 1 3 6 8 9 10\r\nrccb away 4 5 6 10 11 12\r\nrddd home 3 4 5 8 10 12\r\nmw away 2 4 6 8 9 11\r\ntm home 1 5 6 7 8 9\r\ndnbc home 1 2 5 6 8 9\r\ndhc home 1 2 3 5 6 11'
// EXAMPLE TAB
/*
#cat:Friendly
#tag:#1
#date:01.01.2019

A - Team A - 10
Alice 120
Billy 101
Carol 78
Derek 78
Ellen 72
Frank 61

B - Tean B
Grant 102
Henry 40+50
Isaac 80
James 78
Karen 64
Lucas 58/11
Owen 2/1

rsl home 1 2 7 8 11 12
ds home 1 2 3 6 8 12
rmc away 5 6 7 8 9 10
rttc away 5 6 7 8 9 11
dcl home 1 3 4 5 6 7
dyc away 1 3 6 8 9 10
rccb away 4 5 6 10 11 12
rddd home 3 4 5 8 10 12
mw away 2 4 6 8 9 11
tm home 1 5 6 7 8 9
dnbc home 1 2 5 6 8 9
dhc home 1 2 3 5 6 11
*/

export default class TableMaker extends Component {
    constructor(props) {
        super(props);

        this.state = {
            table: _.cloneDeep(INIT_TABLE),
            tracks: {},
            validation: '',
            isLoggedIn: false,
            user: {}
        };

        let state = localStorage['appState'];
        if (state) {
            let AppState = JSON.parse(state);
            this.state.isLoggedIn = AppState.isLoggedIn;
            this.state.user = AppState.user;
        }

        this.signal = axios.CancelToken.source();

        this.convertStringToTable = this.convertStringToTable.bind(this);
        this.parseTeam = this.parseTeam.bind(this);
        this.parsePlayer = this.parsePlayer.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.createImage = this.createImage.bind(this);
        this.loadExample = this.loadExample.bind(this);
        this.save = this.save.bind(this);
        this.saveDiscord = this.saveDiscord.bind(this);
        this.tableViewElement = React.createRef();
    }

    componentDidUpdate() {
        this.tableViewElement.current.updateTable(this.state.table);
    }

    componentDidMount(){
        axios({
            method: 'get',
            url: 'api/tracks',
            cancelToken: this.signal.token,
        })
            .then(response => {
                var tracks = {};
                response.data.data.forEach(function(track) {
                    tracks[track.code] = track.name;
                });

                this.setState(prevState => ({
                    'table' : prevState.table,
                    'tracks' : tracks
                }));
            })
            .catch(function(err) {
                if (axios.isCancel(err)) return;
            });
    }

    componentWillUnmount() {
        this.signal.cancel('Debug: Api is being canceled duo to unmount');
    }

    parseTeam(line){
        var team = line.split(' - ');
        return {
            'name' : team[1] ? team[1].trim() : null,
            'tag' : team[0].trim(),
            'penalty' : team[2] ? team[2].trim() : null,
            'players' : []
        };
    }

    parsePlayer(line){
        let player = line.trim().match(/^(.+) ([0-9+]+)(\/[0-9]+)?$/);
        return {
            'name' : player ? player[1] : line,
            'score' : player && player[2] ? collect(player[2].split('+')).map(points => parseInt(points)).sum() : null,
            'races' : player && player[3] ? parseInt(player[3].replace('/', '')) : null
        }
    }

    convertStringToTable(str) {
        var lines = str.split('\n');
        var current = 0;
        var table = _.cloneDeep(INIT_TABLE);

        // Table params
        var condition = true;
        do {
            if (!lines[current] || !lines[current].includes('#')){ condition = false; break; }

            var param = lines[current].replace('#', '').split(':');

            switch (param[0]){
                case 'tag':
                    table.tag = param[1];
                    break;

                case 'cat' :
                case 'category' :
                    table.category = param[1];
                    break;

                case 'date':
                    table.date = param[1];
                    break;
            }
            current++;
        } while (condition)

        if (lines[current] == ''){ current++; }
        if (!lines[current]) { return table; }

        // Home team
        table.home_team = this.parseTeam(lines[current]);
        current++;

        if (!lines[current]) { return table; }

        // Home players
        var condition = true;
        do {
            table.home_team.players.push(this.parsePlayer(lines[current]));
            current++;
            if (!lines[current] || lines[current] == "") { condition = false; }
        } while(condition);

        current++;
        if (!lines[current]) { return table; }

        // Away Team
        table.away_team = this.parseTeam(lines[current]);
        current++;

        if (!lines[current]) { return table; }

        // Away players
        var condition = true;
        do {
            table.away_team.players.push(this.parsePlayer(lines[current]));
            current++;
            if (!lines[current] || lines[current] == "") { condition = false; }
        } while(condition);

        current++;
        if (!lines[current]) { return table; }

        // Tracks
        var condition = true;
        var no = 1;
        do {
            let race = lines[current].trim().split(' ');

            let points = 0;
            if (race.length == 3) {
                points = parseInt(race[2]);
            }
            else {
                for (let i = 2; i < race.length; i++){
                    points += POINTS[race[i] - 1];
                }
            }

            race = {
                'no' : no,
                'track' : this.state.tracks[race[0].toUpperCase()],
                'vote' : race[1] == 'home' ? 'home' : 'away',
                'home_score' : points,
                'away_score' : points == 0 ? 0 : POINTS_PER_RACE - points
            }
            table.races.push(race);

            no++;
            current++;
            if (!lines[current] || lines[current] == "") { condition = false; }
        } while(condition);

        // Total score
        var homeScore = -table.home_team.penalty;
        var awayScore = -table.away_team.penalty;
        collect(table.races).each(function (race) {
            homeScore += race.home_score;
            awayScore += race.away_score;
        });
        table.home_team.score = homeScore;
        table.away_team.score = awayScore;

        return table;
    }

    handleChange(event){
        var table = this.convertStringToTable(event.target.value);
        var validationMessage = '';

        if (!_.isEqual(table,INIT_TABLE)) {
            var validation = true;

            var homeScoreWRaces = 0;
            var awayScoreWRaces = 0;
            collect(table.races).each(function (race) {
                homeScoreWRaces += race.home_score;
                awayScoreWRaces += race.away_score;
            });
            var totalScoreWRaces = homeScoreWRaces + awayScoreWRaces;


            var homeScoreWPlayers = 0;
            var awayScoreWPlayers = 0;
            collect(table.home_team.players).each(function(player) {
                homeScoreWPlayers += parseInt(player.score);
            });
            collect(table.away_team.players).each(function(player) {
                awayScoreWPlayers += parseInt(player.score);
            });
            var totalScoreWPlayers = homeScoreWPlayers + awayScoreWPlayers;

            if (table.races.length != NB_RACES) {
                validationMessage += `You need to play exactly ${NB_RACES} races for save. ${NB_RACES - table.races.length} left.`;
                validation = false;
            }
            else
            {
                if (totalScoreWRaces != totalScoreWPlayers) {
                    validationMessage += `Total score with players (${totalScoreWPlayers}) does not match to total score with races (${totalScoreWRaces}).`;
                    validation = false;
                }

                if (homeScoreWRaces != homeScoreWPlayers) {
                    if (validationMessage != ''){ validationMessage += '\n'; }
                    validationMessage += `Home score with players (${homeScoreWPlayers}) and races (${homeScoreWRaces}) does not match.`;
                    validation = false;
                }

                if (awayScoreWRaces != awayScoreWPlayers) {
                    if (validationMessage != ''){ validationMessage += '\n'; }
                    validationMessage += `Away score with players (${awayScoreWPlayers}) and races (${awayScoreWRaces}) does not match.`;
                    validation = false;
                }
            }

            if (validation) { validationMessage = 'no-error'; }
        };

        this.setState({'table':table, 'validation' : validationMessage});
    };

    createImage(){
        var node = document.getElementsByClassName('mk-table')[0];

        domtoimage.toPng(node, {width : 600})
        .then(function (dataUrl) {
            var link = document.createElement('a');
            link.download = 'mk-table.png';
            link.href = dataUrl;
            link.click();
        });
    }

    loadExample(){
        var textarea = document.getElementById('textarea-table');
        textarea.value = EXAMPLE_WAR;

        var event = new Event('input', { bubbles: true });
        textarea.dispatchEvent(event);
    }

    save(){
        axios({
            method: 'post',
            url: '/api/wars',
            cancelToken: this.signal.token,
            data: {
                table: this.state.table
            }
        })
        .then(function (response) {
            alert(response.data.message);
        })
        .catch(function(err) {
            if (axios.isCancel(err)) return;
        });
    }

    async saveDiscord(){
        let instance = axios.create();
        instance.defaults.headers.common = {};
        instance.defaults.headers.common.accept = 'application/json';

        let webhookUrl = 'https://discordapp.com/api/webhooks/669137140743012392/uRMSeC7LRaN-3kXPXrziNJal-Rw7j-rZVSYT8uMp4KXN8NCkc1DFXzouKtCnqDtEEIXk';
        let webhook = await instance.request({
            method: 'get',
            url: webhookUrl
        })
        .then(function (response) {
            return response.data;
        })
        .catch(function (error) {
            console.log(error);
        });

        let status = this.state.table.home_team.score > this.state.table.away_team.score ? 'Win' : 'Lose';
        status = this.state.table.home_team.score == this.state.table.away_team.score ? 'Tie' : status;
        let colors = {
            'Win' : 0x168ede,
            'Lose' : 0xf50076,
            'Tie' : 0xffffff
        }

        // NOTE: Using discord.js
        var node = document.getElementsByClassName('mk-table')[0];
        let image = await domtoimage.toPng(node, {width : 600});
        let data = image.replace(/^data:image\/\w+;base64,/, "");
        let buffer = new Buffer(data, 'base64');

        let embed = {
            color: colors[status],
            //url: 'url.to.table.view',
            title: `${this.state.table.category} ${this.state.table.tag}`,
            description: `\`${this.state.table.home_team.name}\` VS \`${this.state.table.away_team.name}\``,
            image: {
                url: 'attachment://table.png',
            },
            fields: [
                {
                    name: 'Date',
                    value: this.state.table.date,
                    inline: true,
                },
                {
                    name: 'Result',
                    value: status,
                    inline: true
                }
            ],
            footer: {
                text: '©MYT'
            }
        };

        const webhookClient = new WebhookClient(webhook.id, webhook.token);
        webhookClient.send('', {
            embeds: [embed],
            //files: [attachment]
            files: [{
                attachment: buffer.buffer,
                name: 'table.png'
            }]
        })
        .then(console.log)
        .catch(console.error);

        return;
        // NOTE: Using HTTP request
        instance.request({
            method: 'post',
            url: `https://discordapp.com/api/v6/webhooks/${webhook.id}/${webhook.token}`,
            data: {
                embeds: [{
                    color: colors[status],
                    //url: 'url.to.table.view',
                    title: `${this.state.table.category} ${this.state.table.tag}`,
                    description: `\`${this.state.table.home_team.name}\` VS \`${this.state.table.away_team.name}\``,
                    /*image: {
                        url: 'url.to.table.view.img',
                    },*/
                    fields: [
                        {
                            name: 'Date',
                            value: this.state.table.date,
                            inline: true,
                        },
                        {
                            name: 'Result',
                            value: status,
                            inline: true
                        }
                    ],
                    footer: {
                        text: '©MYT'
                    }
                }]
            }
        })
        .then(function (response) {
            alert('Result has been sent to discord.')
        })
        .catch(function (error) {
            console.log(error);
        });

    }

    render() {
        return (
            <div className="table-editor">
                <div className="row">
                    <div className="col">
                        <textarea id="textarea-table" onChange={this.handleChange}></textarea>
                    </div>

                    <div className="col mb-2">
                        <TableView table={this.state.table} ref={this.tableViewElement}></TableView>
                    </div>
                </div>

                <div className="btn-group" role="group" aria-label="Basic example">
                    <button className="btn btn-primary-dark" onClick={this.loadExample}>Load example</button>
                    <button id="btn-save" className="btn btn-primary-dark" onClick={this.save} disabled={this.state.validation != 'no-error' || !this.state.isLoggedIn}>Save</button>
                    {/*<button id="btn-save-discord" className="btn btn-primary-dark" onClick={this.saveDiscord} disabled={this.state.validation != 'no-error' || !this.state.isLoggedIn}>Save discord</button>*/}
                    <button className="btn btn-primary-dark" onClick={this.createImage}>Download image</button>
                </div>

                {
                    this.state.validation != 'no-error' && this.state.validation != '' ?
                    <div className="alert alert-danger mt-3" role="alert">
                        <h4 className="alert-heading">Something looks wrong!</h4>
                        <ul>{this.state.validation.split('\n').map((item, index)  => <li key={index}>{item}</li>)}</ul>
                    </div>
                    : ''
                }

            </div>
        );
    }
}
