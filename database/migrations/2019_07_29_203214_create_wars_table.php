<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wars', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->unsignedInteger('category_id');
            $table->string('tag', 20);
            $table->unsignedInteger('home_team_id');
            $table->unsignedInteger('home_score');
            $table->unsignedInteger('home_penalty');
            $table->unsignedInteger('away_team_id');
            $table->unsignedInteger('away_score');
            $table->unsignedInteger('away_penalty');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('category_id')
                ->references('id')->on('categories')
                ->onUpdate('cascade')->onDelete('restrict');

            $table->foreign('home_team_id')
                ->references('id')->on('teams')
                ->onUpdate('cascade')->onDelete('restrict');

            $table->foreign('away_team_id')
                ->references('id')->on('teams')
                ->onUpdate('cascade')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wars');
    }
}
