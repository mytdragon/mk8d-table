import React from 'react';

const NotFound = () => {
    return (
        <footer>
            Not found
        </footer>
    );
};

export default NotFound;
