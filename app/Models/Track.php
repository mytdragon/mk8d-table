<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Exceptions\ModelNotFoundException;

class Track extends Model
{
    protected $table = 'tracks';
    public $timestamps = false;
    protected $fillable = ['code', 'name'];
    protected $guarded = ['id'];

    public function wars() {
        return $this->belongsToMany('App\Models\War', 'wars_tracks')->withPivot('home_score', 'away_score');
    }
}
