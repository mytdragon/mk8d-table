<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarsPlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wars_players', function (Blueprint $table) {
            $table->unsignedInteger('war_id');
            $table->unsignedInteger('player_id');
            $table->unsignedInteger('team_id');
            $table->unsignedInteger('score');
            $table->unsignedInteger('races');

            $table->primary(['war_id', 'player_id']);

            $table->foreign('war_id')
                ->references('id')->on('wars')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('player_id')
                ->references('id')->on('players')
                ->onUpdate('cascade')->onDelete('restrict');

            $table->foreign('team_id')
                ->references('id')->on('teams')
                ->onUpdate('cascade')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wars_players');
    }
}
