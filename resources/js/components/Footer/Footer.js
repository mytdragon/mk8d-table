import React from 'react';

const Footer = () => {
    return (
        <footer className="container">
            <div className="text-center text-monospace font-weight-light">
                <p className="mb-0">Copyright &copy; {new Date().getFullYear()} - <span className="font-weight-bold">{process.env.MIX_DEV_NAME}</span></p>
                <p><a href={`https://twitter.com/${process.env.MIX_DEV_TWITTER}`}>Twitter</a> | <a href={`https://gitlab.com/${process.env.MIX_DEV_GITLAB}`}>Gitlab</a></p>
            </div>
        </footer>
    );
};

export default Footer;
