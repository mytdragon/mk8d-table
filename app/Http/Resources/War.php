<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class War extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'home_team' => [
                'name' => $this->homeTeam->name,
                'tag' => $this->homeTeam->tag,
                'penalty' => $this->home_penalty ? $this->home_penalty : null,
                'score' => $this->home_score,
                'players' => $this->players->filter(function ($player) {
                    return $player->pivot->team_id == $this->homeTeam->id;
                })->values()->map(function ($player) {
                    return [
                        'name' => $player->name,
                        'score' => $player->pivot->score,
                        'races' => $player->pivot->races == config('mk8dx.nb_races') ? null : $player->pivot->races
                    ];
                })
            ],
            'away_team' => [
                'name' => $this->awayTeam->name,
                'tag' => $this->awayTeam->tag,
                'penalty' => $this->away_penalty ? $this->away_penalty : null,
                'score' => $this->away_score,
                'players' => $this->players->filter(function ($player) {
                    return $player->pivot->team_id == $this->awayTeam->id;
                })->values()->map(function ($player) {
                    return [
                        'name' => $player->name,
                        'score' => $player->pivot->score,
                        'races' => $player->pivot->races == config('mk8dx.nb_races') ? null : $player->pivot->races
                    ];
                })
            ],
            'races' => $this->races->sortBy('pivot.no')->map(function ($race) {
                return [
                    'no' => $race->pivot->no,
                    'track' => $race->name,
                    'vote' => $race->pivot->vote_team_id == $this->homeTeam->id ? 'home' : 'away',
                    'home_score' => $race->pivot->home_score,
                    'away_score' => $race->pivot->away_score
                ];
            })->all(),
            'date' => $this->date->format('d.m.Y'),
            'category' => $this->category->name,
            'tag' => $this->tag
        ];
    }
}
