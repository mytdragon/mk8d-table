import React, { Component } from 'react';

import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';

class ActivateConfirm extends Component {

    componentDidMount() {
        setTimeout(function() {
            this.props.history.push('/login');
        }.bind(this), 5000)
    }

    render() {
        return (
            <div>
                <Header userIsLoggedIn={false}/>

                <main className="container py-5">
                    <div className="row justify-content-center">
                        <div className="col-lg-5 col-md-8 col-10">
                            <div className="alert alert-success" role="alert">
                                <h4 className="alert-heading">Account activated!</h4>
                                <p>Your account has been Successfully activated. You can now log in.</p>
                                <hr/>
                                <p className="mb-0">You will be redirected in a few seconds...</p>
                            </div>
                        </div>
                    </div>
                </main>

                <Footer/>
            </div>
        );
    }
}

export default ActivateConfirm;
