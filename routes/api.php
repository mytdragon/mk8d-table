<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'AuthController@login')->name('login');
    Route::post('signup', 'AuthController@signup')->name('signup');
    Route::get('signup/activate/{token}', 'AuthController@signupActivate')->name('activateUser');

    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('logout', 'AuthController@logout')->name('logout');
        Route::get('user', 'AuthController@user')->name('getAuthenticatedUser');
    });

    Route::group(['prefix' => 'password'], function() {
        Route::post('create', 'PasswordResetController@create')->name('createPasswordReset');
        Route::get('find/{token}', 'PasswordResetController@find')->name('findPasswordReset');
        Route::patch('reset', 'PasswordResetController@reset')->name('resetPassword');
    });
});

Route::resource('tracks', 'TrackController')->only(['index', 'show']);
Route::resource('categories', 'CategoryController')->only(['index']);
Route::resource('wars', 'WarController')->only(['index', 'store', 'show', 'update', 'destroy']);
Route::resource('teams', 'TrackController')->only(['index']);
