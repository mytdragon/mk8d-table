import React, { Component } from 'react';
import collect from 'collect.js';
import { std } from 'mathjs';

import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';

export default class Stats extends Component {
    constructor(props) {
        super(props);

        this.state = {
            filter: {
                home_team: null,
                category: null
            },
            tracks: {},
            wars: {},
            teams: collect({}),
            categories: collect({}),
            stats: {},
            isLoading: true,
            isLoggedIn: false,
            user: {}
        };

        let state = localStorage['appState'];
        if (state) {
            let AppState = JSON.parse(state);
            this.state.isLoggedIn = AppState.isLoggedIn;
            this.state.user = AppState.user;
        }

        this.signal = axios.CancelToken.source();

        this.handleFilter = this.handleFilter.bind(this)
        this.makeStats = this.makeStats.bind(this);
        this.tracks = this.tracks.bind(this);
        this.players = this.players.bind(this);
        this.round2Decimals = this.round2Decimals.bind(this);
    }

    componentDidMount(){
        axios({
            method: 'get',
            url: 'api/tracks',
            cancelToken: this.signal.token,
        })
        .then(response => {
            var tracks = {};
            response.data.data.forEach(function(track) {
                tracks[track.name] = {
                    id: track.id
                };
            });

            this.setState(prevState => ({
                tracks : tracks,
                isLoading: prevState.isLoading
            }));
        })
        .catch(function(err) {
            if (axios.isCancel(err)) return;
        });

        axios({
            method: 'get',
            url: 'api/wars',
            cancelToken: this.signal.token,
        })
        .then(response => {
            var wars = collect(response.data.data);

            this.setState(prevState => ({
                tracks : prevState.tracks,
                wars: wars,
                stats: this.makeStats(wars),
                teams: wars.pluck('home_team.name').unique(),
                categories: wars.pluck('category').unique(),
                isLoading: false
            }));
        })
        .catch(function(err) {
            if (!axios.isCancel(err))
                console.log('Fetch Error :c', err);
        });
    }

    componentWillUnmount() {
        this.signal.cancel('Debug: Api is being canceled duo to unmount');
    }

    handleFilter(e) {
        let value = e.target.value;
        let name = e.target.getAttribute('name');

        let filter = {
            ...this.state.filter,
            [name]: value == "0" ?  null : value
        };

        return this.setState(prevState => ({
            ...prevState,
            stats: this.makeStats(prevState.wars, filter),
            filter: filter
        }));
    }

    makeStats(wars, filter = {}) {
        wars = wars.filter(war => {
            if (filter.home_team && war.home_team.name != filter.home_team) {
                return false;
            }

            if (filter.category && war.category != filter.category ) {
                return false;
            }

            return true;
        })

        /** ===== TRACKS =====
            WR FORMULA (  (2 × Wins + Ties) / (2 × Total Games Played) × 100  )
            GLOBAL TRACKS VAR : nbPicksGlobal

            Pick rate (calculated on render)
            Pick rate on home picks (calculated on render)
            nbPicks
            nbPicks home

            Win rate (calculated on render)
            Wins
            Draws
            Loss

            Win rate on home picks (calculated on render)
            Wins home
            Draws home
            Loss home

            Win rate on away picks (calculated on render)
            Wins away
            Draws away
            Loss away

            avg diff (calculated on render)
            avg diff home (calculated on render)
            avg diff away (calculated on render)
            diff
            diff home
            diff away
         */
        var tracks = collect(this.state.tracks).map((track, index) => {
            return {
                id: track.id,
                name: index,
                nb_picks: 0,
                nb_picks_home: 0,
                win: 0,
                draw: 0,
                loss: 0,
                win_home: 0,
                draw_home: 0,
                loss_home: 0,
                win_away: 0,
                draw_away: 0,
                loss_away: 0,
                diff: 0,
                diff_home: 0,
                diff_away: 0
            }
        });

        var nbPicksGlobal = 0;
        var nbPicksHome = 0;
        wars.each((war) => {
            collect(war.races).each((race) => {
                let track = tracks.get(race.track);
                let diff = race.home_score - race.away_score;

                nbPicksGlobal++;
                track.nb_picks++;
                if (race.vote == 'home') {
                    nbPicksHome++;
                    track.nb_picks_home++;
                    if (race.home_score > race.away_score) { track.win++; track.win_home++; track.diff += diff; track.diff_home += diff}
                    else if (race.home_score == race.away_score) { track.draw++; track.draw_home++; }
                    else { track.loss++; track.loss_home++; track.diff -= diff; track.diff_home -= diff }
                }
                else {
                    if (race.home_score > race.away_score) { track.win++; track.win_away++; track.diff -= diff; track.diff_away -= diff }
                    else if (race.home_score == race.away_score) { track.draw++; track.draw_away++; }
                    else { track.loss++; track.loss_away++; track.diff -= diff; track.diff_away -= diff }
                }
            });
        });

        var tracksFinal = [];
        tracks.each( (track, index) => {
            tracksFinal.push(track);
        });
        /** ===== END TRACKS ===== **/

        /** ===== PLAYERS =====
            WR FORMULA (  (2 × Wins + Ties) / (2 × Total Games Played) × 100  )
            AVG ( collect.avg() ) (only wars with 12 races)
            HIGHEST ( collect.max() ) (only wars with 12 races)
            LOWEST ( collect.min() ) (only wars with 12 races)
            Standard deviation formula ( ??? ) (only wars with 12 races)

            Total played
            Wins
            Draws
            Loss

            scores (array) (only if played 12 races)
        */

        var players = {};
        wars.each((war) => {
            collect(war.home_team.players).each((player) => {
                if ( !(player.name in players) ) players[player.name] = {
                    name: player.name,
                    totalPlayed: 0,
                    win: 0,
                    draw: 0,
                    loss: 0,
                    scores: []
                };

                players[player.name]['totalPlayed'] = ++players[player.name]['totalPlayed'];
                if (war.home_team.score > war.away_team.score) ++players[player.name]['win'];
                else if (war.home_team.score < war.away_team.score) ++players[player.name]['loss'];
                else ++players[player.name]['draw'];
                if (!player.races) players[player.name]['scores'].push(player.score);
            });
        });

        var playersFinal = [];

        collect(players).sortKeys().each( (player, index) => {
            playersFinal.push(player);
        });
        /** ===== END PLAYERS ===== **/

        return {
            nbPicksGlobal: nbPicksGlobal,
            nbPicksHome: nbPicksHome,
            tracks: tracksFinal,
            players: playersFinal
        }
    }

    round2Decimals(num) {
        return Math.round( ( num + Number.EPSILON ) * 100 ) / 100;
    }

    tracks(track, index) {
        var nbPicksGlobal = this.state.stats.nbPicksGlobal;
        var nbPicksHome = this.state.stats.nbPicksHome;

        return <tr key={index}>
            <th scope="row">{track.name}</th>
            <td>{this.round2Decimals( (track.nb_picks / nbPicksGlobal) * 100 )}%</td>
            <td>{this.round2Decimals( (track.nb_picks_home / nbPicksHome) * 100 )}%</td>
            <td>{track.nb_picks}</td>
            <td>{track.nb_picks_home}</td>
            <td>{track.nb_picks > 0 ? this.round2Decimals( ( (2 * track.win + track.draw) / (2 * track.nb_picks) ) * 100 ) + '%' : ''}</td>
            <td>{track.win}</td>
            <td>{track.draw}</td>
            <td>{track.loss}</td>
            <td>{track.nb_picks_home > 0 ? this.round2Decimals( ( (2 * track.win_home + track.draw_home) / (2 * track.nb_picks_home) ) * 100 ) + '%' : ''}</td>
            <td>{track.win_home}</td>
            <td>{track.draw_home}</td>
            <td>{track.loss_home}</td>
            <td>{track.nb_picks - track.nb_picks_home > 0 ? this.round2Decimals( ( (2 * track.win_away + track.draw_away) / (2 * (track.nb_picks - track.nb_picks_home) ) ) * 100 ) + '%' : ''}</td>
            <td>{track.win_away}</td>
            <td>{track.draw_away}</td>
            <td>{track.loss_away}</td>
            <td>{track.nb_picks > 0 ? this.round2Decimals( track.diff / track.nb_picks ) : ''}</td>
            <td>{track.nb_picks_home > 0 ? this.round2Decimals( track.diff_home / track.nb_picks_home ) : ''}</td>
            <td>{track.nb_picks - track.nb_picks_home > 0 ? this.round2Decimals( track.diff_away / (track.nb_picks - track.nb_picks_home) ) : ''}</td>
            <td>{track.diff}</td>
            <td>{track.diff_home}</td>
            <td>{track.diff_away}</td>
        </tr>
    }

    players(player, index) {
        return (
            <tr key={index}>
                <th scope="row">{player.name}</th>
                <td>{player.totalPlayed}</td>
                <td>{this.round2Decimals( (2 * player.win + player.draw) / (2 * player.totalPlayed) * 100 )}%</td>
                <td>{player.win}</td>
                <td>{player.draw}</td>
                <td>{player.loss}</td>
                <td>{this.round2Decimals(collect(player.scores).avg())}</td>
                <td>{this.round2Decimals(collect(player.scores).max())}</td>
                <td>{this.round2Decimals(collect(player.scores).min())}</td>
                <td>{this.round2Decimals(  std(player.scores))}</td>
            </tr>
        );
    }

    render() {
        var tracks = !this.state.isLoading ? this.state.stats.tracks.map((track, index) => this.tracks(track, index) )  : 'Loading tracks\'s stats..';
        var players = !this.state.isLoading ? this.state.stats.players.map((player, index) => this.players(player, index) )  : 'Loading players\'s stats..';

        return (
            <div>
                <Header userData={this.state.user} userIsLoggedIn={this.state.isLoggedIn}/>

                <main className="container py-5">
                    <h2>Stats</h2>

                    <div className="form-inline mb-2">
                        <select name="home_team" defaultValue={0} className="form-control mr-sm-2" onChange={(e) => this.handleFilter(e)}>
                            <option value={0}> -- Choose a home team -- </option>
                            {
                                this.state.teams.isNotEmpty() ?
                                    this.state.teams.map(team => <option key={team} value={team}>{team}</option>)
                                : ''
                            }
                        </select>
                        <select name="category" defaultValue={0} className="form-control mr-sm-2" onChange={(e) => this.handleFilter(e)}>
                            <option value={0}> -- Choose a category -- </option>
                            {
                                this.state.categories.isNotEmpty() ?
                                    this.state.categories.map(category => <option key={category} value={category}>{category}</option>)
                                : ''
                            }
                        </select>
                    </div>

                    <div className="table-responsive">
                        <table className="table table-dark table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">Player</th>
                                    <th scope="col">Total played</th>
                                    <th scope="col">Win rate</th>
                                    <th scope="col">Wins</th>
                                    <th scope="col">Draws</th>
                                    <th scope="col">Loss</th>
                                    <th scope="col">Average</th>
                                    <th scope="col">Highest</th>
                                    <th scope="col">Lowest</th>
                                    <th scope="col">Standard deviation</th>
                                </tr>
                            </thead>
                            <tbody>
                                {!this.state.isLoading ? (
                                    players
                                ) : (
                                    <tr><th scope="col"><h3>Loading...</h3></th></tr>
                                )}
                            </tbody>
                        </table>
                    </div>

                    <div className="table-responsive">
                        <table className="table table-dark table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">Race</th>
                                    <th scope="col">Pick rate</th>
                                    <th scope="col">Pick rate on home picks</th>
                                    <th scope="col">nbPicks</th>
                                    <th scope="col">nbPicks home</th>
                                    <th scope="col">Win rate</th>
                                    <th scope="col">Wins</th>
                                    <th scope="col">Draws</th>
                                    <th scope="col">Loss</th>
                                    <th scope="col">Win rate on home picks</th>
                                    <th scope="col">Wins home</th>
                                    <th scope="col">Draws home</th>
                                    <th scope="col">Loss home</th>
                                    <th scope="col">Win rate on away picks</th>
                                    <th scope="col">Wins away</th>
                                    <th scope="col">Draws away</th>
                                    <th scope="col">Loss away</th>
                                    <th scope="col">avg diff</th>
                                    <th scope="col">avg diff home</th>
                                    <th scope="col">avg diff away</th>
                                    <th scope="col">diff</th>
                                    <th scope="col">diff home</th>
                                    <th scope="col">diff away</th>
                                </tr>
                            </thead>
                            <tbody>
                                {!this.state.isLoading ? (
                                    tracks
                                ) : (
                                  <tr><th scope="col"><h3>Loading...</h3></th></tr>
                                )}
                            </tbody>
                        </table>
                    </div>

                </main>

                <Footer/>
            </div>
        );
    }
}
