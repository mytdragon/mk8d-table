<?php

namespace App\Http\Controllers;

use App\Models\Track;
use App\Http\Resources\Track as TrackResource;
use Illuminate\Http\Request;

class TrackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return TrackResource::collection(Track::all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Track  $track
     * @return \Illuminate\Http\Response
     */
    public function show(Track $track)
    {
        return TrackResource($track);
    }
}
