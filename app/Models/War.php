<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class War extends Model
{
    use SoftDeletes;

    protected $table = 'wars';
    protected $fillable = ['tag', 'date', 'home_score', 'home_penalty', 'away_score', 'away_penalty'];
    protected $guarded = ['id'];
    protected $dates = ['date'];

    public function homeTeam() {
        return $this->belongsTo('App\Models\Team');
    }

    public function awayTeam() {
        return $this->belongsTo('App\Models\Team');
    }

    public function players() {
        return $this->belongsToMany('App\Models\Player', 'wars_players')->withPivot('score', 'races', 'team_id');
    }

    public function races() {
        return $this->belongsToMany('App\Models\Track', 'wars_tracks')->withPivot('no', 'home_score', 'away_score', 'vote_team_id');
    }

    public function category() {
        return $this->belongsTo('App\Models\Category');
    }
}
