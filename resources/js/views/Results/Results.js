import React, { Component } from 'react';
import axios from 'axios';

import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import TableView from '../../components/TableView/TableView';

export default class Results extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            wars : {},
            filter: '',
            isLoggedIn: false,
            user: {}
        };

        let state = localStorage['appState'];
        if (state) {
            let AppState = JSON.parse(state);
            this.state.isLoggedIn = AppState.isLoggedIn;
            this.state.user = AppState.user;
        }

        this.signal = axios.CancelToken.source();

        this.handleFilter = this.handleFilter.bind(this)
        this.filter = this.filter.bind(this)
    }

    componentDidMount(){
        axios({
            method: 'get',
            url: 'api/wars',
            cancelToken: this.signal.token,
        })
            .then(response => {
                this.setState(prevState => ({
                    wars : response.data.data,
                    isLoading: false
                }));
            })
            .catch(function(err) {
                if (axios.isCancel(err)) return;
            });

    }

    componentWillUnmount() {
        this.signal.cancel('Debug: Api is being canceled duo to unmount');
    }

    handleFilter(e) {
        let search = e.target.value;

        if (!search.includes(':')){
            return this.setState({
                filter: e.target.value,
            });
        }

        let params = {};
        search.split(';').forEach(element => {
            let x = element.split(/([^:]*):(.*)/);
            params[x[1]] = x[2] ? x[2].split(',') : null;
        })

        return this.setState({
            filter: params,
        });
    };

    filter(war) {
        if (this.state.filter.length) {
            return JSON.stringify(war).includes(this.state.filter);
        }

        if (this.state.filter['id'] && !this.state.filter['id'].includes(war.id.toString()) ) {
            return false;
        }

        if (this.state.filter['cat'] && !this.state.filter['cat'].includes(war.category) ) {
            return false;
        }

        if (this.state.filter['tag'] && !this.state.filter['tag'].includes(war.tag) ) {
            return false;
        }

        if (this.state.filter['date'] && !this.state.filter['date'].includes(war.date) ) {
            return false;
        }

        if (this.state.filter['teams']) {
            let validation = [ war['home_team']['name'], war['away_team']['name'] ].some( name => this.state.filter['teams'].includes(name) );
            if (!validation) { return false; }
        }

        if (this.state.filter['players']) {
            let validation = war['home_team']['players'].concat(war['away_team']['players']).some( player => this.state.filter['players'].includes(player['name']) );
            if (!validation) { return false; }
        }

        if (this.state.filter['tracks']) {
            let validation = war['races'].some( race => this.state.filter['tracks'].includes(race['track']) );
            if (!validation) { return false; }
        }

        return true;
    }

    render() {
        return (
            <div>
                <Header userData={this.state.user} userIsLoggedIn={this.state.isLoggedIn}/>

                <main className="container py-5">
                    <h2>Results</h2>

                    <input type="text" className="form-control mb-3" placeholder="search..." onChange={(e) => this.handleFilter(e)}/>

                    <div className="row">
                        {!this.state.isLoading ? (
                            this.state.wars.filter(war => this.filter(war)).map(war => {
                                return (
                                    <div key={war.id} className="col-12 mb-3">
                                        <TableView table={war}></TableView>
                                    </div>
                                );
                            })
                        ) : (
                            <h3>Loading...</h3>
                        )}
                    </div>
                </main>

                <Footer/>
            </div>
        );
    }
}
