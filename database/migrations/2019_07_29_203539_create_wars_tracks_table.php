<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarsTracksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wars_tracks', function (Blueprint $table) {
            $table->unsignedInteger('war_id');
            $table->unsignedInteger('track_id');
            $table->unsignedInteger('vote_team_id');
            $table->unsignedInteger('no');
            $table->unsignedInteger('home_score');
            $table->unsignedInteger('away_score');

            $table->primary(['war_id', 'track_id']);

            $table->foreign('war_id')
                ->references('id')->on('wars')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('track_id')
                ->references('id')->on('tracks')
                ->onUpdate('cascade')->onDelete('restrict');

            $table->foreign('vote_team_id')
                ->references('id')->on('teams')
                ->onUpdate('cascade')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wars_tracks');
    }
}
