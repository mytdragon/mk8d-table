<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Player extends Model
{
    use SoftDeletes;

    protected $table = 'players';
    protected $fillable = ['name'];
    protected $guarded = ['id'];

    public function wars() {
        return $this->belongsToMany('App\Models\War', 'wars_players');
    }
}
