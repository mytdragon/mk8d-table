import React, {Component} from 'react';
import {Link, Redirect, withRouter} from 'react-router-dom';
import FlashMessage from 'react-flash-message';

import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import LoadingSpinner from '../../components/LoadingSpinner/LoadingSpinner';

class LoginContainer extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoggedIn: false,
            isRegistered: false,
            error: '',
            formSubmitting: false,
            user: {
                email: '',
                password: '',
            },
            showSuccessRegistration: false
        };

        let state = localStorage['appState'];
        if (state) {
            let AppState = JSON.parse(state);
            this.state.isLoggedIn = AppState.isLoggedIn;
            this.state.isRegistered = AppState.isRegistered;
            this.state.user = AppState.user;
        }

        if (this.state.isRegistered) {
            let appState = {
                isLoggedIn: false,
                isRegistered: false,
                user: {}
            };
            localStorage['appState'] = JSON.stringify(appState);

            this.state.showSuccessRegistration = true;
            this.state.isRegistered = false;
        }

        if (this.state.isLoggedIn) return this.props.history.push('/');

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.loadingSpinner = React.createRef();
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({formSubmitting: true});
        this.loadingSpinner.current.show();

        let userData = this.state.user;
        axios.post("/api/auth/login", userData)
        .then(response => {
            return response;
        })
        .then(json => {
            let userData = {
                id: json.data.id,
                name: json.data.name,
                email: json.data.email,
                access_token: json.data.access_token,
            };

            let appState = {
                isLoggedIn: true,
                user: userData
            };
            localStorage['appState'] = JSON.stringify(appState);

            this.setState({
                isLoggedIn: appState.isLoggedIn,
                user: appState.user,
                error: ''
            });

            this.loadingSpinner.current.hide();

            return location.reload();
        })
        .catch(error => {
            if (error.response) {
                // The request was made and the server responded with a status code that falls out of the range of 2xx
                let err = error.response.data;

                this.setState({
                    error: err.message,
                    errorMessage: err.errors,
                    formSubmitting: false
                })
            }
            else if (error.request) {
                // The request was made but no response was received `error.request` is an instance of XMLHttpRequest in the browser and an instance of http.ClientRequest in node.js
                let err = error.request;
                this.setState({
                    error: err,
                    formSubmitting: false
                })
            }
            else {
                // Something happened in setting up the request that triggered an Error
                let err = error.message;
                this.setState({
                    error: err,
                    formSubmitting: false
                })
            }

            this.loadingSpinner.current.hide();
        })
        .finally(this.setState({error: ''}));
    }

    handleChange(e) {
        let name = e.target.name;
        let value = e.target.value;
        this.setState(prevState => ({
            user: {
                ...prevState.user,
                [name]: value
            }
        }));
    }

    render() {
        /*const { state = {} } = this.state.redirect;
        const { error } = state;*/

        return (
            <div>
                <Header userData={this.state.user} userIsLoggedIn={false}/>

                <main className="container py-5">
                    <div className="row justify-content-center">
                        <div className="col-lg-5 col-md-8 col-10">
                            <h2>Log in to your account</h2>

                            {
                                this.state.error ?
                                    <FlashMessage duration={100000} persistOnHover={true}>
                                        <div className={"alert alert-danger"}>
                                            Error: {this.state.error}
                                        </div>
                                    </FlashMessage>
                                : ''
                            }
                            {/*
                                error && !this.state.isLoggedIn ?
                                    <FlashMessage duration={100000} persistOnHover={true}>
                                        <div className={"alert alert-danger"}>
                                            Error: {error}
                                        </div>
                                    </FlashMessage>
                                : ''
                            */}
                            {
                                this.state.showSuccessRegistration ?
                                <FlashMessage duration={10000} persistOnHover={true}>
                                    <div className={"alert alert-success"}>
                                        You have been successfully registered. Please activate your account with the link on the email we sent before you log in.
                                    </div>
                                </FlashMessage>
                                : ''
                            }

                            <form className="modern-form" onSubmit={this.handleSubmit}>
                                <div className="form-group">
                                    <input id="email" type="email" name="email" placeholder="E-mail" className="form-control" required onChange={this.handleChange}/>
                                </div>
                                <div className="form-group">
                                    <input id="password" type="password" name="password" placeholder="Password" className="form-control" required onChange={this.handleChange}/>
                                </div>
                                <button disabled={this.state.formSubmitting} type="submit" name="singlebutton" className="btn btn-primary-dark  btn-block" style={{display: this.state.formSubmitting ? "none" : "block"}}>Log in</button>
                                <LoadingSpinner ref={this.loadingSpinner}/>
                            </form>

                            <p>
                                Don't have an account? <Link to="/register">Register</Link>
                            </p>
                        </div>
                    </div>
                </main>

                <Footer/>
            </div>
        )
    }
}

export default withRouter(LoginContainer);
