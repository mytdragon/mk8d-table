import React, { Component } from 'react';
import './LoadingSpinner.css';

class LoadingSpinner extends Component {

    constructor() {
        super();

        this.state = {
            visibility: false
        };
    }

    toggle() {
        this.setState({visibility: !this.state.visibility})
    }

    show() {
        this.setState({visibility: true})
    }

    hide() {
        this.setState({visibility: false})
    }

    render() {
        return (
            <div className="loading-spinner" style={{display: this.state.visibility ? "block" : "none"}}></div>
        );
    }

}

export default LoadingSpinner;
