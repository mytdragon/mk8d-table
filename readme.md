# mk8d-table
## Installation
1. copy `.env.example` to `.env` and config
1. `composer install`
1. `php artisan migrate`
1. `php artisan db:seed`
1. `php artisan key:generate`
1. `php artisan passport:install`
1. `npm install`

<!-- Suite guide auth: https://medium.com/@000kelvin/setting-up-laravel-and-react-js-the-right-way-using-user-authentication-1cfadf3194e -->
