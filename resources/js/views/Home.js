import React, {Component} from 'react';

import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';
import TableMaker from '../components/TableMaker/TableMaker';

class Home extends Component {

    constructor() {
        super();

        this.state = {
            isLoggedIn: false,
            user: {}
        }

        let state = localStorage['appState'];
        if (state) {
            let AppState = JSON.parse(state);
            this.state.isLoggedIn = AppState.isLoggedIn;
            this.state.user = AppState.user;
        }
    }

    render() {
        return (
            <div>
                <Header userData={this.state.user} userIsLoggedIn={this.state.isLoggedIn}/>

                <main className="container py-5">
                    <TableMaker/>
                </main>

                <Footer/>
            </div>
        );
    }
}

export default Home;
