import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import Main from './Router';

import 'bootstrap/dist/css/bootstrap.min.css';
import './main.css';

export default class Index extends Component {
    render() {
        return (
            <BrowserRouter>
                <Route component={Main} />
            </BrowserRouter>
        );
    }
}

if (document.getElementById('root')) {
    ReactDOM.render(<Index />, document.getElementById('root'));
}
