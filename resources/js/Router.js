import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';

import Login from './views/Login/Login';
import Register from './views/Register/Register';
import ActivateConfirm from './views/Register/ActivateConfirm';
import Home from './views/Home';
import NotFound from './views/NotFound';
import Results from './views/Results/Results';
import Stats from './views/Stats/Stats';

import PrivateRoute from './PrivateRoute';

const Main = props => (
    <Switch>
        {/*Auth*/}
        <Route path='/login' component={Login}/>
        <Route path='/register' component={Register}/>
        <Route path='/activate-confirmation' component={ActivateConfirm}/>

        {/*Pages*/}
        <Route exact path='/' component={Home}/>
        <PrivateRoute path='/results' component={Results}/>
        <PrivateRoute path='/stats' component={Stats}/>

        {/*Errors*/}
        <Route component={NotFound}/>
    </Switch>
);

export default Main;
