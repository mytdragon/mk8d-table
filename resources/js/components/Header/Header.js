import React, { Component } from 'react';
import {Link, NavLink, withRouter} from 'react-router-dom';

class Header extends Component {

    constructor(props) {
        super(props);

        this.state = {
            user: props.userData,
            isLoggedIn: props.userIsLoggedIn
        };
        this.logOut = this.logOut.bind(this);
    }

    logOut() {
        let appState = {
            user: {},
            isLoggedIn: false
        };
        localStorage['appState'] = JSON.stringify(appState);
        this.setState(appState);
    }

    render() {
        const aStyle = {
            cursor: 'pointer'
        };

        return (
            <nav className="navbar navbar-expand-md navbar-dark bg-dark">
                <Link to="/" className="navbar-brand">{process.env.MIX_APP_NAME}</Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarNav">
                    <div className="navbar-nav">
                        <NavLink exact className="nav-item nav-link" to="/" activeClassName="active">Table maker</NavLink>
                        {
                            this.state.isLoggedIn ?
                                [
                                    <NavLink key={0} className="nav-item nav-link" to="/results" activeClassName="active">Results</NavLink>,
                                    <NavLink key={1} className="nav-item nav-link" to="/stats" activeClassName="active">Stats</NavLink>
                                ]
                            : ""
                        }
                    </div>

                    <div className="navbar-nav ml-auto">
                    {
                        !this.state.isLoggedIn ?
                            [
                                <NavLink key={0} className="nav-item nav-link" to="/login" activeClassName="active">Login</NavLink>,
                                <NavLink key={1} className="nav-item nav-link" to="/register" activeClassName="active">Register</NavLink>
                            ]
                        : [
                            <NavLink key={0} className="nav-item nav-link" onClick={this.logOut} to="/">Logout</NavLink>
                        ]
                    }
                    </div>
                </div>
            </nav>
        );
    }
}

/*

<nav className="navbar">
    <ul>
        {this.state.isLoggedIn ?
            <li className="has-sub"><Link to="/dashboard">Dashboard</Link></li> : ""}
        {!this.state.isLoggedIn ?
            <li><Link to="/login">Login</Link> | <Link to="/register">Register</Link></li> : ""}
    </ul>
</nav>

*/

export default withRouter(Header);
